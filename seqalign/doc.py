#===============================================================================
# doc.py
#===============================================================================

# Constants ====================================================================

DOC = """There are two commands for aligning sequencing data:
`seqalign-chip-seq` and `seqalign-atac-seq`. See their
respective documentation by running:

seqalign-chip-seq --help
seqalign-atac-seq --help
"""




# Functions ====================================================================

def main():
    print(DOC, end='')
