# seqalign

Manage sequence alignments

## Installation

```sh
pip install seqalign
```
or
```sh
pip install --user seqalign
```

If you have not already installed [picardtools](https://github.com/anthony-aylward/picardtools),
you will also need to run:
```sh
picardtools-download
```

## Examples

```python
with SequenceAlignment(<path to input BAM or FASTQ file>) as sa:
    sa.cleans_up_bam = False
    sa.remove_supplementary_alignments()
    sa.samtools_sort(memory_limit=10)
    sa.samtools_index()
    sa.write(<path to output BAM file>)
```

## Notes

The `input_file` argument to `SequenceAlignment()` should be a string for
single-end reads or for data that is already aligned. For raw paired-end reads,
it should be a tuple containing two strings giving the paths to the two
FASTA / FASTQ files.

## Command line usage

`seqalign` includes a couple of pre-built pipelines for aligning ChIP-seq and ATAC-seq data.
They rely on certain environment variables being set, as described [here](https://github.com/anthony-aylward/chipatac)
To see the basic command line documentation, simply enter `seqalign` at the terminal:

```
seqalign
```
```
There are two commands for aligning sequencing data:
`seqalign-chip-seq` and `seqalign-atac-seq`. See their
respective documentation by running:

seqalign-chip-seq --help
seqalign-atac-seq --help
```
